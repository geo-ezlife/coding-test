import { useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';
import { RadialChart, Hint } from 'react-vis';
import Button from '@material-ui/core/Button';

const DashboardPage = (props) => {
  const history = useHistory();
  const [countries, setCountries] = useState([]);
  const [summaryData, setSummaryData] = useState([
    {
      TotalRecords: '',
      AgeAverage: '',
      RateAverage: '',
      GenderM: '',
      GenderF: '',
    },
  ]);
  const [selectedCountry, setSelectedCountry] = useState({});

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    const countryReq = await fetch(
      'https://localhost:5100/api/dashboard/countryDistribution'
    );
    const countryResp = await countryReq.json();
    setCountries(countryResp);

    const summaryReq = await fetch(
      'https://localhost:5100/api/dashboard/summary'
    );
    const summaryResp = await summaryReq.json();
    setSummaryData(summaryResp);
  };

  return (
    <div>
      <h1>Dashboard</h1>
      <div className='dashboard-container'>
        <div className='summary-content'>
          <h2>Summary</h2>
          <p>
            <strong>Total Surveys </strong>
            {summaryData[0].TotalRecords}
          </p>
          <p>
            <strong>Age Average </strong> {summaryData[0].AgeAverage}
          </p>
          <p>
            <strong>Rate Avarage </strong> {summaryData[0].RateAverage}
          </p>
          <div>
            <strong>Gender</strong>
            <div style={{ margin: '15px' }}>
              <p>Male: {summaryData[0].GenderM}</p>
              <p>Female: {summaryData[0].GenderF}</p>
              <p>
                Unknown:
                {summaryData[0].TotalRecords -
                  (summaryData[0].GenderF + summaryData[0].GenderM)}
              </p>
            </div>
          </div>
        </div>
        <div className='summary-content'>
          <h2>Country Distribution</h2>
          <p>Hover mouse on the chart to see data</p>
          <RadialChart
            className={'donut-chart-example'}
            innerRadius={100}
            radius={130}
            getAngle={(d) => d.Records}
            data={countries}
            onValueMouseOver={(v) => setSelectedCountry(v)}
            onSeriesMouseOut={(v) => setSelectedCountry(false)}
            width={300}
            height={300}
            padAngle={0.04}
          >
            {selectedCountry !== false && <Hint value={selectedCountry} />}
          </RadialChart>
        </div>
      </div>

      <Button
        variant='contained'
        color='primary'
        onClick={() => {
          history.push({
            pathname: '/survey',
            state: { origin: 'dashboard' },
          });
        }}
      >
        Go to Survey
      </Button>
    </div>
  );
};
export default DashboardPage;
