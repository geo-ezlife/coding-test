import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const AdminPage = (props) => {
  const location = useLocation();
  let history = useHistory();

  useEffect(() => {}, [location]);

  return (
    <div>
      <h1>Admin</h1>
      <div>
        <Button
          variant='contained'
          color='primary'
          style={{ margin: '10px' }}
          fullWidth
          onClick={() => {
            fetch('https://localhost:5100/api/admin/tools/insertDummyData', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'Access-Control-Allow-Origin': 'http://localhost:3000',
                'Access-Control-Allow-Credentials': 'true',
              },
            }).then((response) =>
              history.push({
                pathname: '/dashboard',
              })
            );
          }}
        >
          Insert dummy data
        </Button>
      </div>
      <Button
        variant='contained'
        fullWidth
        style={{ margin: '10px', backgroundColor: 'red  ' }}
        color='secondary'
        onClick={() => {
          fetch('https://localhost:5100/api/admin/tools/deleteAllSurveyData', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              'Access-Control-Allow-Origin': 'http://localhost:3000',
              'Access-Control-Allow-Credentials': 'true',
            },
          }).then((response) =>
            history.push({
              pathname: '/dashboard',
            })
          );
        }}
      >
        Delete all survey data
      </Button>
    </div>
  );
};
export default AdminPage;
