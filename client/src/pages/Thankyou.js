import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const ThankyouPage = (props) => {
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    console.log(location.state.detail); // result: 'some_value'
  }, [location]);

  return (
    <div>
      <h1>Thanks {location.state.detail.name}</h1>
      <Button
        variant='contained'
        color='primary'
        onClick={() => {
          history.push({
            pathname: '/' + location.state.detail.origin,
          });
        }}
      >
        Go back to {location.state.detail.origin} page
      </Button>
    </div>
  );
};
export default ThankyouPage;
