import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';

const HomePage = (props) => {
  const location = useLocation();
  let history = useHistory();
  useEffect(() => {}, [location]);

  return (
    <div>
      <h1>Home</h1>
      <Button
        variant='contained'
        color='primary'
        onClick={() => {
          history.push({
            pathname: '/survey',
            state: { origin: 'home' },
          });
        }}
      >
        Go to Survey
      </Button>
    </div>
  );
};
export default HomePage;
