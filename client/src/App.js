import React from 'react';
import logo from './logo.svg';

import SurveyPage from './pages/Survey';
import ThankyouPage from './pages/Thankyou';
import HomePage from './pages/Home';
import AboutPage from './pages/About';
import AdminPage from './pages/Admin';
import DashboardPage from './pages/Dashboard';

import Button from '@material-ui/core/Button';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';

const theme = createMuiTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      main: '#1ad334',
    },
    secondary: {
      // This is green.A700 as hex.
      main: '#11cb5f',
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <header className="header-container">
        <div className='logo-container'>
          <img src={logo} alt='logo' className='logo'></img>
        </div>
        <Router>
          <div>
            <nav>
              <ul className='menu'>
                <li>
                  <Button variant='contained' color='primary'>
                    <Link to='/' className='menu-button-text'>
                      Home
                    </Link>
                  </Button>
                </li>
                <li>
                  <Button variant='contained' color='primary'>
                    <Link to='/about' className='menu-button-text'>
                      About
                    </Link>
                  </Button>
                </li>
                <li>
                  <Button variant='contained' color='primary'>
                    <Link to='/dashboard' className='menu-button-text'>
                      Dashboard
                    </Link>
                  </Button>
                </li>
                <li>
                  <Button variant='contained' color='primary'>
                    <Link to='/admin' className='menu-button-text'>
                      Admin
                    </Link>
                  </Button>
                </li>
              </ul>
            </nav>
          </div>
          <div>
            <Switch>
              <Route path='/thankyou'>
                <ThankyouPage />
              </Route>
              <Route path='/dashboard'>
                <DashboardPage />
              </Route>
              <Route path='/about'>
                <AboutPage />
              </Route>
              <Route path='/survey'>
                <SurveyPage />
              </Route>
              <Route path='/admin'>
                <AdminPage />
              </Route>
              <Route path='/'>
                <HomePage />
              </Route>
            </Switch>
          </div>
        </Router>
      </header>
    </ThemeProvider>
  );
}

export default App;
