var dbConfig = require('../config/db');
var sql = require('mssql');

async function getDashboardSummary() {
  try {
    const pool = await sql.connect(dbConfig);
    const summary = pool.request().query(
      `SELECT COUNT(*) AS TotalRecords, 
              AVG(age) AS AgeAverage,
              AVG(rate) AS RateAverage, 
              SUM(CASE WHEN gender = 'F' THEN 1 ELSE 0 END) AS GenderF,
              SUM(CASE WHEN gender = 'M' THEN 1 ELSE 0 END) AS GenderM
        FROM dbo.surveys`
    );
    return summary;
  } catch (err) {
    console.log(err);
  }
}

async function getCountryDistribution() {
  try {
    const pool = await sql.connect(dbConfig);
    const distribution = pool.request().query(
      `SELECT country AS Country,
              count(*) AS Records,
              AVG(age) AS AgeAverage,
              AVG(rate) AS RateAverage,
              SUM(CASE WHEN gender = 'F' THEN 1 ELSE 0 END) AS GenderF,
              SUM(CASE WHEN gender = 'M' THEN 1 ELSE 0 END) AS GenderM
        FROM dbo.surveys
        GROUP BY country
      `
    );
    return distribution;
  } catch (err) {
    console.log(err);
  }
}

module.exports = {
  getDashboardSummary: getDashboardSummary,
  getCountryDistribution: getCountryDistribution,
};
