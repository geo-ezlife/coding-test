var dbConfig = require('../config/db');
var sql = require('mssql');

async function insertDummyData() {
  try {
    const pool = await sql.connect(dbConfig);
    await pool.request().execute('API_InsertDummyData');
  } catch (err) {
    console.log(err);
  }
}

async function deleteAllSurveyData() {
  try {
    const pool = await sql.connect(dbConfig);
    await pool.request().execute('API_DeleteAllSurveyData');
  } catch (err) {
    console.log(err);
  }
}

module.exports = {
  insertDummyData: insertDummyData,
  deleteAllSurveyData: deleteAllSurveyData,
};
