var dbConfig = require('../config/db');
var sql = require('mssql');

async function getSurveys() {
  try {
    const pool = await sql.connect(dbConfig);
    const surveys = pool.request().query('SELECT * FROM surveys');
    return surveys;
  } catch (err) {
    console.error(err);
  }
}

async function addSurvey(survey) {
  try {
    const pool = await sql.connect(dbConfig);
    await pool
      .request()
      .input('name', sql.NVarChar, survey.name)
      .input('mail', sql.NVarChar, survey.mail)
      .input('age', sql.Int, survey.age)
      .input('gender', sql.Char, survey.gender)
      .input('country', sql.NVarChar, survey.country)
      .input('rate', sql.Int, survey.rate)
      .input('suggestions', sql.Text, survey.suggestions)
      .input('origin', sql.NVarChar, survey.origin)
      .execute('API_InsertSurvey');
  } catch (err) {
    console.log(err);
  }
}

module.exports = {
  getSurveys: getSurveys,
  addSurvey: addSurvey,
};
