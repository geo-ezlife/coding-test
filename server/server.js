// Core libraries
const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const https = require('https');
const fs = require('fs');
const bodyParser = require('body-parser');

// Load config
dotenv.config({ path: './config/config.env' });
const app = express();
const PORT = process.env.PORT || 5100;

// Swagger
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerOptions = require('./config/swaggerConfig');
const specs = swaggerJsdoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const router = require('./routes/routes');
app.use('/api', router);
app.use(router);

var httpsOptions = {
  key: fs.readFileSync('C:/Windows/System32/localhost-key.pem'),
  cert: fs.readFileSync('C:/Windows/System32/localhost.pem'),
  requestCert: false,
  rejectUnauthorized: false,
};

const server = https.createServer(httpsOptions, app).listen(PORT, () => {
  console.log(
    `Server running in ${process.env.NODE_ENV} mode. Port used: ${PORT}`
  );
});
