const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Lokky Surveys API with Swagger',
      version: '0.1.0',
      description:
        'A simple API application made with Express and documented with Swagger',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Lokky',
        url: 'https://lokky.it',
        email: 'george161193@gmail.com',
      },
    },
    servers: [
      {
        url: 'https://localhost:5100/api',
      },
    ],
  },
  apis: ["./routes/*.js"],
};
module.exports = options;
