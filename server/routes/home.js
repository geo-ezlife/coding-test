// Homepage
exports.page = function (req, res) {
  res.send(`
  <h1>Lokky Survey API for coding test</h1>
  <a href="/api-docs">Go to the swagger page of the API</a>
  `);
};
