const express = require('express');
const router = express.Router();

router.use((req, resp, next) => {
  console.log(`Called ${req.baseUrl}${req.path} by IP: ${req.ip}`);
  next();
});

// Index page
const home = require('./home');
router.get('/', home.page);

// Surveys
const surveys = require('./surveys');
router.get('/surveys', surveys.getSurveys);
router.post('/surveys', surveys.insertSurvey);

// Admin
const admin = require('./admin');
router.post('/admin/tools/insertDummyData', admin.insertDummyData);
router.post('/admin/tools/deleteAllSurveyData', admin.deleteAllSurveyData);

// Dashboard
const dashboard = require('./dashboard');
router.get('/dashboard/summary', dashboard.getDashboardSummary);
router.get('/dashboard/countryDistribution', dashboard.getCountryDistribution);

module.exports = router;
