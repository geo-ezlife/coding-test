const _surveyService = require('../services/survey.service');

// Get All surveys
exports.getSurveys = function (req, res) {
  _surveyService.getSurveys().then((result) => {
    if (result.recordset) res.json(result.recordset);
  });
};

// Insert new survey
exports.insertSurvey = function (req, resp) {
  const survey = { ...req.body };
  _surveyService.addSurvey(survey).then((result) => {
    resp.status(201).json(result);
  });
};
