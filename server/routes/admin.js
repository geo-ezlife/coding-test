const _adminService = require('../services/admin.service');

exports.insertDummyData = function (req, res) {
  _adminService.insertDummyData().then((result) => {
    res.status(201).json(result);
  });
};

exports.deleteAllSurveyData = function (req, res) {
  _adminService.deleteAllSurveyData().then((result) => {
    res.status(201).json(result);
  });
};
