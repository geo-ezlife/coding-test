/**
 * @swagger
 * components:
 *   schemas:
 *     Survey:
 *       type: object
 *       required:
 *         - name
 *         - mail
 *       properties:
 *         id:
 *           type: number
 *           description: The auto-generated id of the survey
 *         name:
 *           type: string
 *           description: The name of the person who took the survey
 *         mail:
 *           type: string
 *           description: The mail of the person who took the survey
 *         age:
 *           type: number
 *           description: The age of the person who took the survey
 *         gender:
 *           type: string
 *           description: The gender of the person who took the survey
 *         country:
 *           type: string
 *           description: The country of the person who took the survey
 *         rate:
 *           type: number
 *           description: The experience rate assigned by the person who did the survey
 *         suggestions:
 *           type: string
 *           description: Suggestions given by the person who took the survey
 *         origin:
 *           type: string
 *           description: The page the user came from
 *         insertDate:
 *           type: string
 *           description: The DateTime of the submitted survey
 *       example:
 *         id: 11
 *         name: Aroush Humphrey
 *         mail: crypt@optonline.net
 *         age: 47
 *         gender: M
 *         country: United Kingdom
 *         rate: 4
 *         suggestions: Lorem ipsum dolor sit amet
 *         origin: homepage
 *         insertDate: 2021-03-26 16:42:15.520
 *     SurveyInput:
 *       type: object
 *       required:
 *         - name
 *         - mail
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the person who took the survey
 *         mail:
 *           type: string
 *           description: The mail of the person who took the survey
 *         age:
 *           type: number
 *           description: The age of the person who took the survey
 *         gender:
 *           type: string
 *           description: The gender of the person who took the survey
 *         country:
 *           type: string
 *           description: The country of the person who took the survey
 *         rate:
 *           type: number
 *           description: The experience rate assigned by the person who did the survey
 *         suggestions:
 *           type: string
 *           description: Suggestions given by the person who took the survey
 *         origin:
 *           type: string
 *           description: The page the user came from
 *       example:
 *         name: Aroush Humphrey
 *         mail: crypt@optonline.net
 *         age: 47
 *         gender: M
 *         country: United Kingdom
 *         rate: 4
 *         suggestions: Lorem ipsum dolor sit amet
 *         origin: homepage
 *
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     DashboardSummary:
 *       type: object
 *       properties:
 *         TotalRecords:
 *           type: number
 *           description: Total records present in survey table
 *         AgeAverage:
 *           type: number
 *           description: Age average of all users present in survey table
 *         RateAverage:
 *           type: number
 *           description: Rate average of all users present in survey table
 *         GenderM:
 *           type: number
 *           description: Total number of male present in survey table
 *         GenderF:
 *           type: number
 *           description: Total number of female present in survey table
 *       example:
 *         TotalRecords: 100
 *         AgeAverage: 43
 *         RateAverage: 4
 *         GenderM: 49
 *         GenderF: 51
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     DashboardCountryDistribution:
 *       type: object
 *       properties:
 *         Country:
 *           type: string
 *           description: Country
 *         Records:
 *           type: number
 *           description: Records of the country present in survey table
 *         AgeAverage:
 *           type: number
 *           description: Age average for the country present in survey table
 *         RateAverage:
 *           type: number
 *           description: Rate average for the country present in survey table
 *         GenderM:
 *           type: number
 *           description: Number of male who took the survey for the country in survey table
 *         GenderF:
 *           type: number
 *           description: Number of female who took the survey for the country in survey table
 *       example:
 *         Country: Italy
 *         TotalRecords: 40
 *         AgeAverage: 47
 *         RateAverage: 3
 *         GenderM: 25
 *         GenderF: 22
 */

/**
 * @swagger
 * tags:
 *   name: Surveys
 *   description: Lokky surveys endpoint
 */

/**
 * @swagger
 * tags:
 *   name: Admin
 *   description: Lokky admin endpoint
 */

/**
 * @swagger
 * tags:
 *   name: Dashboard
 *   description: Lokky dashboard endpoint
 */

/**
 * @swagger
 * /surveys:
 *   get:
 *     summary: Returns the list of all the surveys
 *     tags: [Surveys]
 *     responses:
 *       200:
 *         description: The list of the surveys
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Survey'
 *   post:
 *     summary: Insert a new Survey
 *     tags: [Surveys]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/SurveyInput'
 */

/**
 * @swagger
 * /admin/tools/insertDummyData:
 *   post:
 *     summary: Insert a dummy dataset
 *     tags: [Admin]
 * /admin/tools/deleteAllSurveyData:
 *   post:
 *     summary: Delete all survey data in DB
 *     tags: [Admin]
 */

/**
 * @swagger
 * /dashboard/summary:
 *   get:
 *     summary: Returns the summary of all the surveys
 *     tags: [Dashboard]
 *     responses:
 *       200:
 *         description: The summary of all records present in suvrey table
 *         content:
 *           application/json:
 *             schema:
 *               items:
 *                 $ref: '#/components/schemas/DashboardSummary'
 * /dashboard/countryDistribution:
 *   get:
 *     summary: Returns the country distribution of the records present in survey table
 *     tags: [Dashboard]
 *     responses:
 *       200:
 *         description: The country distribution of all records present in suvrey table
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               items:
 *                 $ref: '#/components/schemas/DashboardCountryDistribution'
 */
