const _dashboardService = require('../services/dashboard.service');

// Get Dashboard summary
exports.getDashboardSummary = function (req, res) {
  _dashboardService.getDashboardSummary().then((result) => {
    if (result.recordset) res.json(result.recordset);
  });
};

// Get Country distribution
exports.getCountryDistribution = function (req, res) {
  _dashboardService.getCountryDistribution().then((result) => {
    if (result.recordset) res.json(result.recordset);
  });
};
